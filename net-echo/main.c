#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <poll.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdbool.h>

#define socket_error -1
#define max_connections 1
static const int flag_on = 1;
static const int buf_size = 64 * 1024; // v bytech
const struct timeval rx_tout = {0L, 400000L}; // sekundy, mikrosekundy

/*
 * Sockety jsou pouzity v blokujicim rezimu, pro prijem je nastaven timeout.
 */

// nastaveni velikosti prijimaciho a vysilaciho buferu a prijimaciho timeoutu
bool
set_socket(int sock)
{
    if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &buf_size,
        sizeof (int)) < 0)
        return (false);
    if (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &buf_size,
        sizeof (int)) < 0)
        return (false);
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &rx_tout,
        sizeof (struct timeval)) < 0)
        return (false);
    return (true);
}

// otevreni a konfigurace TCP socketu
int
tcp_socket(unsigned short port)
{
    if (port == 0)
        return (socket_error);
    // otevreni TCP socketu pro IPv4 i IPv6
    int tcp = socket(AF_INET6, SOCK_STREAM, 0);
    if (tcp < 0)
        return (socket_error);
    // povolit vicenasobne pouziti adresy
    if (setsockopt(tcp, SOL_SOCKET, SO_REUSEADDR, &flag_on,
        sizeof (flag_on)) < 0)
        return (socket_error);
    // nastaveni IP adresy (libovolna) a portu
    struct sockaddr_in6 address;
    memset(&address, 0, sizeof (address));
    address.sin6_family = AF_INET6;
    address.sin6_port = htons(port);
    address.sin6_addr = in6addr_any;
    if (bind(tcp, (struct sockaddr *)(&address), sizeof (address)) < 0)
        return (socket_error);
    // nastaveni socketu do naslouchaciho stavu
    if (listen(tcp, max_connections) < 0 || !set_socket(tcp))
        return (socket_error);
    // platny socket
    return (tcp);
}

// otevreni a konfigurace UDP socketu
int
udp_socket(unsigned short port)
{
    if (port == 0)
        return (socket_error);
    // otevreni UDP socketu pro IPv4 i IPv6
    int udp = socket(AF_INET6, SOCK_DGRAM, 0);
    if (udp < 0)
        return (socket_error);
    // povolit vicenasobne pouziti adresy
    if (setsockopt(udp, SOL_SOCKET, SO_REUSEADDR, &flag_on,
        sizeof (flag_on)) < 0)
        return (socket_error);
    // nastaveni IP adresy (libovolna) a portu
    struct sockaddr_in6 address;
    memset(&address, 0, sizeof (address));
    address.sin6_family = AF_INET6;
    address.sin6_port = htons(port);
    address.sin6_addr = in6addr_any;
    if (bind(udp, (struct sockaddr *)(&address), sizeof (address)) < 0 ||
        !set_socket(udp))
        return (socket_error);
    // platny socket
    return (udp);
}

// obsluha pri detekci spojeni po TCP
bool
tcp_probe(int tcp, size_t *received, struct sockaddr_storage *address)
{
    // prijeti spojeni a nastaveni socketu
    socklen_t addr_len = sizeof (struct sockaddr_storage);
    int sock = accept(tcp, (struct sockaddr *)(address), &addr_len);
    if (sock < 0 || !set_socket(sock))
        return (false);
    // prijem dat ze socketu, pri timeoutu ukonceno
    bool result = true;
    char buffer[buf_size];
    *received = 0;
    while (true) {
        // prijem dat
        ssize_t rx_bytes = recv(sock, buffer, sizeof (buffer), 0);
        if (rx_bytes == 0)
            break; // ukonceni pri uzavreni socketu klientem
        else if (rx_bytes < 0) {
            result = errno == EAGAIN || errno == EWOULDBLOCK;
                // zadna data neprijata
            break;
        }
        *received += rx_bytes;
        // vyslani dat (echo)
        ssize_t tx_bytes = send(sock, buffer, rx_bytes, 0);
        if (tx_bytes == 0)
            break;
        else if (tx_bytes < 0) {
            result = false; // chyba odeslani dat
            break;
        }
    }
    close(sock); // uzavreni pouziteho socketu
    return (result);
}

// obsluha pri detekci spojeni po UDP
bool
udp_probe(int udp, size_t *received, struct sockaddr_storage *address)
{
    char buffer[buf_size];
    *received = 0;
    // prijem dat ze socketu, pri timeoutu ukonceno
    // adresa pro odpoved je zjistena z prijateho paketu
    while (true) {
        // prijem dat
        socklen_t addr_len = sizeof (struct sockaddr_storage);
        ssize_t rx_bytes = recvfrom(udp, buffer, sizeof (buffer), 0,
            (struct sockaddr *)(address), &addr_len);
        if (rx_bytes < 0)
            return (errno == EAGAIN || errno == EWOULDBLOCK);
                // zadna data neprijata
        *received += rx_bytes;
        // vyslani dat (echo)
        ssize_t tx_bytes = sendto(udp, buffer, rx_bytes, 0,
            (struct sockaddr *)(address), addr_len);
        if (tx_bytes < 0)
            return (false); // chyba odeslani dat
    }
}

// vypsani informaci o protistrane - IP adresa a port
void
print_peer(const struct sockaddr_storage *address)
{
    char buffer[100];
    switch (((const struct sockaddr *)(address))->sa_family) {
    case AF_INET: {
            const struct sockaddr_in *ipv4;
            ipv4 = (const struct sockaddr_in *)(address);
            printf("%s:%d", inet_ntop(AF_INET, &ipv4->sin_addr,
                buffer, sizeof (buffer)), ntohs(ipv4->sin_port));
        }
        break;
    case AF_INET6: {
            const struct sockaddr_in6 *ipv6;
            ipv6 = (const struct sockaddr_in6 *)(address);
            printf("[%s]:%d", inet_ntop(AF_INET6, &ipv6->sin6_addr,
                buffer, sizeof (buffer)), ntohs(ipv6->sin6_port));
        }
        break;
    }
}

static const unsigned num_socks = 2;
static const unsigned short tcp_default = 10000;
static const unsigned short udp_default = 10000;
static const char waiting_string[] = "Waiting for connection ... \
(Ctrl-C to quit)";
static const struct option opt_table[] = {
    {"tcp", required_argument, 0, 't'},
    {"udp", required_argument, 0, 'u'},
    {"help", no_argument, 0, 'h'},
    {0, 0, 0, 0}
};

/*
 * Sitove echo pro TCP a UDP - hlavni program
 *
 * Aplikace je pouze jednovlaknova, pozadavky na zpracovani TCP a
 * UDP  prichazeji postupne. Zjisteni spojeni je reseno pomoci
 * (nekonecne) blokujiciho 'poll'. Preruseni uzivatelem je mozne
 * pomoci SIGTERM (Ctrl-C z terminalu).
 *
 * Server nasloucha na vsech dostupnych adresach (loopback i lokalni
 * adresy vsech sitovych adapteru), porty pro TCP i UDP se
 * konfiguruji z prikazove radky.
 */

int
main(int argc, char *argv[])
{
    unsigned i;

    printf("Network echo for benchmark (TCP/UDP, IPv4/IPv6)\n");
    // zpracovani argumentu z prikazove radky
    unsigned short tcp_port = tcp_default;
    unsigned short udp_port = udp_default;
    while (true) {
        int option = getopt_long(argc, argv, "t:u:h", opt_table, NULL);
        if (option == -1)
            break;
        switch (option) {
        case 't':
            tcp_port = (unsigned short)(strtoul(optarg, NULL, 10));
            break;
        case 'u':
            udp_port = (unsigned short)(strtoul(optarg, NULL, 10));
            break;
        default: // help nebo chyba prepinacu
            printf("  Usage: %s {options}\n", argv[0]);
            printf("  Options:\n");
            printf("    -tNNN,  --tcp=NNN  set TCP port to NNN \
                (default 10000)\n");
            printf("    -uNNN,  --udp=NNN  set UDP port to NNN \
                (default 10000)\n");
            printf("    -h,     --help     show this usage page\n");
            return (EXIT_FAILURE);
        }
    }
    // vytvoreni socketu
    int socks[num_socks];
    socks[0] = tcp_socket(tcp_port);
    socks[1] = udp_socket(udp_port);
    // kontrola, zda existuje nejaky platny socket pro prijem spojeni
    bool ok = false;
    if (socks[0] == socket_error)
        printf("  error opening TCP port %d\n", tcp_port);
    else {
        printf("  listening on TCP port %d\n", tcp_port);
        ok = true;
    }
    if (socks[1] == socket_error)
        printf("  error opening UDP port %d\n", udp_port);
    else {
        printf("  listening on UDP port %d\n", udp_port);
        ok = true;
    }
    if (!ok)
        return (EXIT_FAILURE); // zadny port pro prijem nebyl otevren
    // data pro testovani prichoziho spojeni
    struct pollfd poll_data[num_socks];
    for (i = 0; i < num_socks; i++) {
        poll_data[i].fd = socks[i];
        poll_data[i].events = POLLIN;
        poll_data[i].revents = 0;
    }
    // hlavni smycka (prerusi se Ctrl-C)
    printf("%s\n", waiting_string);
    while (poll(poll_data, num_socks, -1) > 0) {
            // blokujici cekani na spojeni
        printf("Connection detected ...\n");
        size_t received;
        struct sockaddr_storage address;
        // nalezeni "ziveho" socketu a volani zpracovatelske funkce
        for (i = 0; i < num_socks; i++) {
            if (poll_data[i].revents == POLLIN) { // socket ma data
                switch (i) {
                case 0: // TCP echo
                    ok = tcp_probe(socks[i], &received, &address);
                    printf("  on TCP port from node ");
                    break;
                case 1: // UDP echo
                    ok = udp_probe(socks[i], &received, &address);
                    printf("  on UDP port from node ");
                    break;
                }
                // vypis vysledku
                print_peer(&address);
                printf("\n");
                if (ok)
                    printf("  %ld bytes echoed\n", received);
                else
                    printf("  operation failed\n");
                break;
            } else if (poll_data[i].revents != 0) { // chyba na socketu
                printf("  connection failed\n");
                break;
            }
        }
        // dalsi kolo
        printf("%s\n", waiting_string);
    }
    return (EXIT_FAILURE);
}
