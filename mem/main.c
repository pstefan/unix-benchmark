#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "../include/test.h"


struct params_t {
    int items;
    int seed;
};

static struct params_t params;
static struct test_t tests[2];
static bool verbose;

static sem_t *parent_sem;
static sem_t *child_sem;
static sem_t *notify_sem;
static uint8_t *child_buffer = NULL;
static uint8_t *data = NULL;
static uint8_t *rec_data = NULL;
static unsigned buf_size = 0;
static const char *last_error;
static void *shared_buffer_p;
int shmid;

static struct test_suite suite;




void
init_2()
{
    buf_size = 2 * 1024;
}

void
init_32()
{
    buf_size = 32 * 1024;
}

const char *
get_info_2()
{
    return ("mem2 - 2kB shared memory");
}

const char *
get_info_32()
{
    return ("mem32 - 32kB shared memory");
}

bool
prepare_environment()
{
    if ((parent_sem = sem_open("mem_sem_par", O_CREAT, 0644, 1)) ==
        SEM_FAILED) {
        last_error = "create semaphore 1 failed";
        return (false);
    }
    if ((child_sem = sem_open("mem_sem_child", O_CREAT, 0644, 0)) ==
        SEM_FAILED) {
        sem_close(parent_sem);
        last_error = "create semaphore 2 failed";
        return (false);
    }
    if ((notify_sem = sem_open("mem_sem_notify", O_CREAT, 0644, 0)) ==
        SEM_FAILED) {
        sem_close(parent_sem);
        sem_close(notify_sem);
        last_error = "create semaphore 2 failed";
        return (false);
    }
    child_buffer = (uint8_t *)malloc(buf_size * sizeof (uint8_t));
    data = (uint8_t *)malloc(params.items * sizeof (uint8_t));
    rec_data = (uint8_t *)malloc(params.items * sizeof (uint8_t));

    if (child_buffer == NULL || data == NULL || rec_data == NULL) {
        last_error = "allocation data failed";
        free(child_buffer);
        free(data);
        free(rec_data);
        child_buffer = data = rec_data = NULL;
        sem_close(parent_sem);
        sem_close(child_sem);
        return (false);
    }
    srand(params.seed);
    int i;
    for (i = 0; i < params.items; i++) {
        data[i] = (uint8_t)rand();
    }

    shmid = shmget(IPC_PRIVATE, buf_size * sizeof (uint8_t),
        0666 | IPC_CREAT);
    if (shmid == -1) {
        last_error = "failed to create shared memory block";
        free(child_buffer);
        free(data);
        free(rec_data);
        sem_close(parent_sem);
        sem_close(child_sem);
        return (false);
    }

    int child;
    if ((child = fork()) == -1) {
        last_error = "fork failed";
        return (false);
    } else if (child == 0) { // child
        int received_total = 0;
        void *shared_buffer;

        free(data);
        free(rec_data);
        if ((shared_buffer = shmat(shmid, 0, 0)) == (void *)(-1)) {
            last_error = "failed to attach shared memory from child";
            return (false);
        }
        // pocka se na notifikaci od hlavniho procesu z run_test();
        sem_wait(notify_sem);

        while (received_total < params.items) {
            // unsigned i;

            sem_wait(child_sem);
            memcpy((void *)child_buffer, shared_buffer, buf_size);
            received_total += buf_size;
            // jenom pruchod for-cyklem zpusobuje znacne zpomaleni.
            // Odkomentujte si pro kontrolu spravnosti (zde a v check
            // _environment
            // for(i = 0; i < buf_size; i++) {
            //   child_buffer[i] <<= 1;
            // }
            memcpy(shared_buffer, (void *)child_buffer, buf_size);
            sem_post(parent_sem);
        }

        // pocka se na druhou notifikaci z clear_environment()
        // pote se provede uklid
        sem_wait(notify_sem);

        sem_close(parent_sem);
        sem_close(child_sem);
        sem_close(notify_sem);
        shmdt(shared_buffer);
        free(child_buffer);
        exit(0);
    } else { // parent
        // nyni nic nedelame
    }

    if (verbose) {
        printf("  data size: %dB\n", params.items);
        printf("  block size: %dB\n", buf_size);
    }
    return (true);
}

void
run_test()
{
    // parent thread

    int send_total = 0;
    uint8_t *temp_data = data;
    uint8_t *temp_rec_data = rec_data;

    // pred zapisem do sdileneho bufferu musim cekat
    // na tomto semaforu
    sem_wait(parent_sem);
    sem_post(notify_sem);

    if ((shared_buffer_p = shmat(shmid, 0, 0)) == (void *)(-1)) {
        last_error = "failed to attach shared memory from child";
        sem_post(parent_sem);
        return;
    }
    memcpy(shared_buffer_p, (void *)temp_data, buf_size);
    temp_data += buf_size;
    send_total += buf_size;
    sem_post(child_sem);

    while (send_total < params.items) {
        sem_wait(parent_sem);
        memcpy((void *)temp_rec_data, shared_buffer_p, buf_size);
        temp_rec_data += buf_size;
        memcpy(shared_buffer_p, (void *)temp_data, buf_size);
        temp_data += buf_size;
        send_total += buf_size;
        sem_post(child_sem);
    }
    sem_wait(parent_sem);
    memcpy((void *)temp_rec_data, shared_buffer_p, buf_size);
    sem_post(parent_sem);

}

void
evaluate(double time)
{
    printf("Evaluated:\n  speed: %f Mbit/s\n",
        (params.items / 1024 / 1024 * 2 * 8) / time);
}

bool
check_result()
{
    // unsigned i;
    // for (i = 0; i < params.items; i++) {
    //    if ((uint8_t)(data[i] << 1) != rec_data[i]) {
    //        return (false);
    //    }
    // }
    return (memcmp(data, rec_data, params.items) == 0);
    // return (true);
}

const char *
get_last_error()
{
    return (last_error);
}

bool
clear_environment()
{
    // podruhe notifikujeme childa -> provede uklid
    sem_post(notify_sem);
    shmdt(shared_buffer_p);
    wait(NULL);
    shmctl(shmid, IPC_RMID, NULL);
    free(child_buffer);
    free(data);
    free(rec_data);
    sem_close(parent_sem);
    sem_close(child_sem);
    sem_close(notify_sem);
    return (true);
}

void
deinit() {}


void
mem_print_params()
{
    printf("   * Params:\n       items: %d\n       seed: %d\n",
        params.items, params.seed);
}


struct test_suite *
create(bool verbose_flag, config_t *cfg)
{
    if (!config_lookup_int(cfg, "mem_items", &params.items)) {
        params.items = 8388608; // 8 MB
    }
    if (!config_lookup_int(cfg, "mem_seed", &params.seed)) {
        params.seed = 4568;
    }

    struct test_t *test;
    test = tests;

    // mem_2
    test->init = init_2;
    test->check_result = check_result;
    test->clear_environment = clear_environment;
    test->evaluate = evaluate;
    test->get_info = get_info_2;
    test->get_last_error = get_last_error;
    test->prepare_environment = prepare_environment;
    test->run_test = run_test;
    test->deinit = deinit;

    test++;

    // mem_32
    test->init = init_32;
    test->check_result = check_result;
    test->clear_environment = clear_environment;
    test->evaluate = evaluate;
    test->get_info = get_info_32;
    test->get_last_error = get_last_error;
    test->prepare_environment = prepare_environment;
    test->run_test = run_test;
    test->deinit = deinit;

    verbose = verbose_flag;

    suite.name = "mem - shared memory interprocess communication";
    suite.is_valid_conf = true;
    suite.test_count = 2;
    suite.print_params = mem_print_params;
    suite.tests = tests;

    return (&suite);
}
