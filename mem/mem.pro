TEMPLATE = lib
TARGET = bm_mem
CONFIG += plugin no_plugin_name_prefix

DESTDIR = ../debug

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lpthread

QMAKE_CXXFLAGS += -fPIC
QMAKE_LFLAGS += -shared -pthread

SOURCES += main.c

HEADERS += \
    ../include/test.h \
    ../include/libconfig.h
