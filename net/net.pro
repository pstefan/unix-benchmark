TEMPLATE = lib
TARGET = bm_net
CONFIG += plugin no_plugin_name_prefix

DESTDIR = ../debug

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    socket.c \
    common.c

HEADERS += \
    socket.h\
    ../include/test.h \
    common.h \
    ../include/libconfig.h

