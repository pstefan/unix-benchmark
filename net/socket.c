#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "socket.h"

/*
 * Sockety jsou pouzity v blokovacim rezimu, pro cteni je nastaven timeout.
 * Vysilaci a prijimaci buffer jsou nastaveny na hodnotu 64 kB, pro UDP
 * je vsak i teoreticke maximum pro funkce 'send' a 'recv' o neco mensi.
 *
 * Z hlediska spojeni funkcionality pro TCP (connection-oriented) a UDP
 * (connection-less) protokoly je i v pripade UDP pouzita funkce 'connect'
 * (zapamatuje se adresu, port atd. protilehle strany) a pro manipulaci
 * s daty pak funkce 'send' a 'recv'. To je pro UDP protokol netypicke,
 * ale mozne (obvykle je 'connect' vynechat a pouzivat 'sendto' a 'recvfrom').
 * Vyhodne je i to, ze pripadne datagramy z jinych adres jsou odfiltrovany.
 *
 * Socket je oteviran s moznosti pouziti IPv4 i IPv6 (je-li implementovano a
 * povoleno). IPv4 adresy jsou mapovany do IPv6 rozsahu dle normy.
 */

// BSD tweak
#ifndef AI_V4MAPPED
#define AI_V4MAPPED 0
#endif

static const int invalid = -1;
const unsigned buf_size = 64 * 1024; // 64 kB
const struct timeval rx_tout = {0L, 500000L}; // sekundy, mikrosekundy

void
soc_cli_init(int *soc)
{
    if (soc != NULL) {
        *soc = invalid;
    }
}

void
soc_cli_deinit(int *soc)
{
    if (soc != NULL && *soc != invalid) {
        close(*soc);
    }
}

unsigned
soc_cli_buffer_size()
{
    return (buf_size);
}

// otevreni socketu pro komunikaci - urceno nazvem protistrany,
// portem a protokolem
bool
soc_cli_open(int *soc, const char *server, unsigned short port,
    enum prot_type type)
{
    if (soc != NULL) {
        if (*soc != invalid)
            return (false); // socket je jiz otevren
        // nalezeni adresy (pripadne vice adres) serveru
        struct addrinfo hints;
        memset(&hints, 0, sizeof (struct addrinfo));
        hints.ai_family = AF_UNSPEC; // moznost IPv4 i IPv6
        hints.ai_socktype = (int)(type); // SOCK_TYPE nebo SOCK_DGRAM
        hints.ai_protocol = 0; // neomezeno
        hints.ai_flags = AI_ADDRCONFIG | AI_V4MAPPED;
        char pport[50];
        struct addrinfo *address_set;
        sprintf(pport, "%d", port);
        if (getaddrinfo(server, pport, &hints, &address_set) < 0)
            return (false);
        // otevreni socketu a navazani spojeni
        // 'getaddrinfo' muze vratit vice moznych adres, procazime je postupne
        // do prvniho uspechu (takovy socket je pouzit pro spojeni)
        struct addrinfo *i;
        for (i = address_set; i != NULL; i = i->ai_next) {
            *soc = socket(i->ai_family, i->ai_socktype, i->ai_protocol);
            if (*soc != invalid) {
                if (connect(*soc, i->ai_addr, i->ai_addrlen) == 0) {
                    // nastaveni socketu (velikost vysilaciho a prijimaciho
                    // buferu a timeoutu pro prijem)
                    setsockopt(*soc, SOL_SOCKET, SO_RCVBUF,
                        (const char *)(&buf_size), sizeof (int));
                    setsockopt(*soc, SOL_SOCKET, SO_SNDBUF,
                        (const char *)(&buf_size), sizeof (int));
                    setsockopt(*soc, SOL_SOCKET, SO_RCVTIMEO,
                        (const char *)(&rx_tout), sizeof (struct timeval));
                    break;
                }
                close(*soc);
                *soc = invalid;
            }
        }
        freeaddrinfo(address_set);
        return (*soc != invalid);
    }
    else
        return (false);
}

// odeslani dat po otevrenem socketu
bool
soc_cli_send(int *soc, const void *data, unsigned length)
{
    if (soc != NULL) {
        if (*soc == invalid || length > buf_size)
            return (false);
        return (send(*soc, data, (size_t)length, 0) == (ssize_t)(length));
    } else {
        return (false);
    }
}

// prijem dat z otevreneho socketu
// 'received' je velikost skutecne prijatych dat
bool
soc_cli_receive(int *soc, void *data, unsigned length, unsigned *received)
{
    if (soc != NULL) {
        *received = 0;
        if (*soc == invalid || length > buf_size)
            return (false);
        ssize_t rx = recv(*soc, data, length, 0);
        if (rx > 0) { // prijata platna data
            *received = (unsigned)(rx);
            return (true);
        } else // timeout nebo chyba socketu
            return (false);
    } else {
        return (false);
    }
}

// uzavreni socketu
void
soc_cli_close(int *soc)
{
    if (soc != NULL) {
        if (*soc != invalid) {
            close(*soc);
            *soc = invalid;
        }
    }
}
