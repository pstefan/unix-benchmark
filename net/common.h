#ifndef SOC_COMMON_H
#define SOC_COMMON_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

struct params_t {
    int seed;
    int test_size;
    int ping_count;
    int ping_data;
    const char *server;
    int tcp_port;
    int udp_port;
};

// deskriptor socketu
int client_;
uint8_t *rx_data_;
uint8_t *tx_data_;
size_t data_size_;
size_t packet_;
bool result_;
const char *last_error_;

static const struct timespec wait_close = {0, 300000000L};
bool verbose;
struct params_t params;

extern void net_test_init(size_t data_size, size_t packet);
extern bool net_test_prepare_environment();
extern void net_test_run_test();
extern void net_test_evaluate(double time);
extern bool net_test_check_result();
extern const char *net_test_get_last_error();
extern bool net_test_clear_environment();
extern void net_test_deinit();

#endif
