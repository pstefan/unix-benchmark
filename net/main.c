#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/test.h"
#include "socket.h"
#include "common.h"

#define TEST_COUNT 7

static struct test_t tests[TEST_COUNT];

static struct test_suite suite;



// -----TCP-----
void
tcp_2_init()
{
    net_test_init(params.test_size, 2 * 1024);
}

const char *
tcp_2_get_info()
{
    return ("tcp_2 - data echoing on TCP with 2 KB blocks");
}

void
tcp_8_init()
{
    net_test_init(params.test_size, 8 * 1024);
}

const char *
tcp_8_get_info()
{
    return ("tcp_8 - data echoing on TCP with 8 KB blocks");
}

void
tcp_32_init()
{
    net_test_init(params.test_size, 32 * 1024);
}

const char *
tcp_32_get_info()
{
    return ("tcp_32 - data echoing on TCP with 32 KB blocks");
}


bool
tcp_prepare_environment()
{
    result_ = true;
    net_test_prepare_environment();
    if (verbose) {
        printf("  server: %s:%d\n", params.server, params.tcp_port);
    }
    if (!soc_cli_open(&client_, params.server, params.tcp_port, tcp)) {
        result_ = false;
        last_error_ = "Error opening socket.";
    }
    return (result_);
}

void
tcp_run_test()
{
    net_test_run_test();
}

void
tcp_evaluate(double time)
{
    net_test_evaluate(time);
}

bool
tcp_check_result()
{
    return (net_test_check_result());
}

const char *
tcp_get_last_error()
{
    return (net_test_get_last_error());
}

bool
tcp_clear_environment()
{
    return (net_test_clear_environment());
}

void
tcp_deinit()
{
    net_test_deinit();
}


// -----UDP-----
void
udp_2_init()
{
    net_test_init(params.test_size, 2 * 1024);
}

const char *
udp_2_get_info()
{
    return ("udp_2 - data echoing on UDP with 2 KB blocks");
}

void
udp_8_init()
{
    net_test_init(params.test_size, 8 * 1024);
}

const char *
udp_8_get_info()
{
    return ("udp_8 - data echoing on UDP with 8 KB blocks");
}

void
udp_32_init()
{
    net_test_init(params.test_size, 32 * 1024);
}

const char *
udp_32_get_info()
{
    return ("udp_32 - data echoing on UDP with 32 KB blocks");
}


bool
udp_prepare_environment()
{
    net_test_prepare_environment();
    if (verbose) {
        printf("  server: %s:%d\n", params.server, params.udp_port);
    }
    if (!soc_cli_open(&client_, params.server, params.udp_port, udp)) {
        result_ = false;
        last_error_ = "Error opening socket.";
    }
    return (result_);
}

void
udp_run_test()
{
    net_test_run_test();
}

void
udp_evaluate(double time)
{
    net_test_evaluate(time);
}

bool
udp_check_result()
{
   return (net_test_check_result());
}

const char *
udp_get_last_error()
{
    return (net_test_get_last_error());
}

bool
udp_clear_environment()
{
    return (net_test_clear_environment());
}

void
udp_deinit()
{
    return (net_test_deinit());
}


void
latency_init()
{
    net_test_init(params.ping_count * params.ping_data, params.ping_data);
}

const char *
latency_get_info()
{
    return ("latency - data echoing on UDP with configured (64 B) blocks");
}

bool
latency_prepare_environment()
{
    net_test_prepare_environment();
    if (verbose) {
        printf("  server: %s:%d\n  ping count: %d\n  ping data: %dB\n",
            params.server, params.udp_port, params.ping_count,
            params.ping_data);
    }
    if (!soc_cli_open(&client_, params.server, params.udp_port, udp)) {
        result_ = false;
        last_error_ = "Error opening socket.";
    }
    return (result_);
}

void
latency_run_test()
{
    net_test_run_test();
}

void
latency_evaluate(double time)
{
    printf("Evaluated:\n  latency: %f seconds\n",
        time / (data_size_ / packet_));
        // data_size_ / packet_ je pocet opakovani
}

bool
latency_check_result()
{
   return (net_test_check_result());
}

const char *
latency_get_last_error()
{
    return (net_test_get_last_error());
}

bool
latency_clear_environment()
{
    return (net_test_clear_environment());
}

void
latency_deinit()
{
    return (net_test_deinit());
}



void
net_print_params()
{
    printf("   * Params:\n       test size: %dB\n       ping count: \
%d\n       ping data: %dB\n       server: %s\n       tcp port: \
%d\n       udp port: %d\n", params.test_size, params.ping_count,
        params.ping_data, params.server, params.tcp_port, params.udp_port);
}


struct test_suite *
create(bool verbose_flag, config_t *cfg)
{
    if (!config_lookup_int(cfg, "net_seed", &params.seed)) {
        params.seed = 4560;
    }
    if (!config_lookup_int(cfg, "net_ping_count", &params.ping_count)) {
        params.ping_count = 4096;
    }
    if (!config_lookup_int(cfg, "net_ping_data", &params.ping_data)) {
        params.ping_data = 64;
    }
    if (!config_lookup_int(cfg, "net_tcp_port", &params.tcp_port)) {
        params.tcp_port = 10000;
    }
    if (!config_lookup_int(cfg, "net_udp_port", &params.udp_port)) {
        params.udp_port = 10000;
    }
    if (!config_lookup_int(cfg, "net_test_size", &params.test_size)) {
        params.test_size = 8388608;
    }
    if (!config_lookup_string(cfg, "net_server", &params.server)) {
        params.server = "localhost";
    }

    struct test_t *test;
    test = tests;

    // latency
    test->check_result = latency_check_result;
    test->clear_environment = latency_clear_environment;
    test->deinit = latency_deinit;
    test->evaluate = latency_evaluate;
    test->get_info = latency_get_info;
    test->get_last_error = latency_get_last_error;
    test->init = latency_init;
    test->prepare_environment = latency_prepare_environment;
    test->run_test = latency_run_test;

    test++;

    // tcp_2
    test->check_result = tcp_check_result;
    test->clear_environment = tcp_clear_environment;
    test->deinit = tcp_deinit;
    test->evaluate = tcp_evaluate;
    test->get_info = tcp_2_get_info;
    test->get_last_error = tcp_get_last_error;
    test->init = tcp_2_init;
    test->prepare_environment = tcp_prepare_environment;
    test->run_test = tcp_run_test;

    test++;

    // udp_2
    test->check_result = udp_check_result;
    test->clear_environment = udp_clear_environment;
    test->deinit = udp_deinit;
    test->evaluate = udp_evaluate;
    test->get_info = udp_2_get_info;
    test->get_last_error = udp_get_last_error;
    test->init = udp_2_init;
    test->prepare_environment = udp_prepare_environment;
    test->run_test = udp_run_test;

    test++;

    // tcp_8
    test->check_result = tcp_check_result;
    test->clear_environment = tcp_clear_environment;
    test->deinit = tcp_deinit;
    test->evaluate = tcp_evaluate;
    test->get_info = tcp_8_get_info;
    test->get_last_error = tcp_get_last_error;
    test->init = tcp_8_init;
    test->prepare_environment = tcp_prepare_environment;
    test->run_test = tcp_run_test;

    test++;

    // udp_8
    test->check_result = udp_check_result;
    test->clear_environment = udp_clear_environment;
    test->deinit = udp_deinit;
    test->evaluate = udp_evaluate;
    test->get_info = udp_8_get_info;
    test->get_last_error = udp_get_last_error;
    test->init = udp_8_init;
    test->prepare_environment = udp_prepare_environment;
    test->run_test = udp_run_test;

    test++;

    // tcp_32
    test->check_result = tcp_check_result;
    test->clear_environment = tcp_clear_environment;
    test->deinit = tcp_deinit;
    test->evaluate = tcp_evaluate;
    test->get_info = tcp_32_get_info;
    test->get_last_error = tcp_get_last_error;
    test->init = tcp_32_init;
    test->prepare_environment = tcp_prepare_environment;
    test->run_test = tcp_run_test;

    test++;

    // udp_32
    test->check_result = udp_check_result;
    test->clear_environment = udp_clear_environment;
    test->deinit = udp_deinit;
    test->evaluate = udp_evaluate;
    test->get_info = udp_32_get_info;
    test->get_last_error = udp_get_last_error;
    test->init = udp_32_init;
    test->prepare_environment = udp_prepare_environment;
    test->run_test = udp_run_test;

    verbose = verbose_flag;

    suite.name = "net - network tests";
    suite.is_valid_conf = true;
    suite.test_count = TEST_COUNT;
    suite.print_params = net_print_params;
    suite.tests = tests;

    return (&suite);
}
