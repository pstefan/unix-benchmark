// spolecne vlastnosti sitoveho testu

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "common.h"
#include "socket.h"





void
net_test_init(size_t data_size, size_t packet) {
    rx_data_ = NULL;
    tx_data_ = NULL;
    data_size_ = data_size;
    packet_ = packet;
    result_ = true;
    last_error_ = NULL;
}

bool
net_test_prepare_environment()
{
    soc_cli_init(&client_);
    // alokace buferu pro vysilani a prijem dat
    rx_data_ = (uint8_t *)malloc(data_size_ * sizeof (uint8_t));
    tx_data_ = (uint8_t *)malloc(data_size_ * sizeof (uint8_t));
    // naplneni vysilaciho buferu nahodnymi daty
    srand(params.seed);
    unsigned i;
    for (i = 0; i < data_size_; i++) {
        tx_data_[i] = (uint8_t)rand();
    }
    if (verbose) {
        printf("  data size: %ldB\n", data_size_);
        printf("  blocks: %ld\n", data_size_ / packet_);
    }
    result_ = true;
    return (true);
}

void
net_test_run_test()
{
    // Ve smycce jsou odesilana a prijimana data po paketech urcene velikosti.
    // Vyjimecne muze nastat situace, kdy prijimaci bufer ma mene dat
    // nez velikost paketu, zbyvajici data jsou nactena pri dalsim cteni.
    // Celkova velikost nactenych dat je kontrolovana a musi odpovidat
    // odeslanym datum.
    size_t sent = 0;
    size_t received = 0;
    unsigned rx_now;
    while (received < data_size_) {
        if (sent < data_size_) {
            // predpokladame, ze celkova velikost dat je delitelna
            // velikosti paketu
            if (!soc_cli_send(&client_, tx_data_ + sent, packet_)) {
                result_ = false;
                last_error_ = "Error sending data to socket.";
                return;
            }
            sent += packet_;
        }
        if (!soc_cli_receive(&client_, rx_data_ + received, packet_, &rx_now)) {
            result_ = false;
            last_error_ = "Error receiving data from socket.";
            return;
        }
        received += rx_now;
    }
}

void
net_test_evaluate(double time)
{
    printf("Evaluated:\n  speed: %f Mbit/s\n",
        (data_size_ / 1024 / 1024 * 2 * 8) / time);
    // data_size_ je v Bytech, tj. data_size_ / 1024 / 1024 je v MB.
    // *2 je protoze data_size_ Bytu odesilam i prijimam, *8 je prevod
    // na bity. Po vydeleni casem mam pocet megabitu za sekundu.
}

bool
net_test_check_result()
{
    if (!result_) // test, zda jiz nastala chyba
        return (false);
    // kontrola porovnanim vyslanych a prijatych dat
    if (memcmp(rx_data_, tx_data_, data_size_) == 0)
        return (true);
    else {
        last_error_ = "Error comparing rx/tx data.";
        return (false);
    }
}

const char *
net_test_get_last_error()
{
    return (last_error_);
}

bool
net_test_clear_environment()
{
    // uvolneni pameti
    free(rx_data_);
    rx_data_ = NULL;
    free(tx_data_);
    tx_data_ = NULL;
    // pauza pro spravnou cinnost echo serveru (rozeznani dalsiho testu)
    nanosleep(&wait_close, NULL);
    soc_cli_close(&client_);
    return (true);
}

void
net_test_deinit()
{
    free(rx_data_);
    free(tx_data_);
    soc_cli_deinit(&client_);
}
