#ifndef SOCKET_H
#define SOCKET_H

#include <stdbool.h>
#include <sys/socket.h>


enum prot_type {tcp = SOCK_STREAM, udp = SOCK_DGRAM};


extern void soc_cli_init(int *soc);
extern void soc_cli_deinit(int *soc);
extern unsigned soc_cli_buffer_size();
extern bool soc_cli_open(int *soc, const char *server,
    unsigned short port, enum prot_type type);
extern bool soc_cli_send(int *soc, const void *data,
    unsigned length);
extern bool soc_cli_receive(int *soc, void *data, unsigned length,
    unsigned *received);
extern void soc_cli_close(int *soc);

#endif
