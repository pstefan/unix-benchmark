####################################################################
# projekt 'benchmark'
####################################################################
#
# Výsledkem sestavení projektu jsou soubor hlavní aplikace, knihov-
# ny pro sady jednotlivých testů. Soubory 'benchmark' a knihovny modulu
# (např. 'bm_example.so' pro modul 'example') jsou umístěny v adresáři
# 'DESTDIR'. Zdrojové a hlavičkové soubory pro hlavní aplikaci jsou
# v adresáři 'benchmark', pro jednotlivé moduly v adresáři dle jména
# modulu. Objektové soubory jsou vytvářeny v podadresářích 'DESTDIR'
# ('benchmark' pro hlavní aplikaci a pro moduly dle jejich jména).
#
####################################################################
# konfigurační část
####################################################################
#
# nástroj pro překlad a linkování
#
CC = gcc
#
# adresář pro výsledné zkompilované soubory
#
DESTDIR = bin
#
# společné příznaky pro kompilaci a linkování modulů
#
SO_CFLAGS = -Wall -O3 -fPIC -c
SO_LFLAGS = -shared -Wl,-rpath='$$ORIGIN'
#
# hlavičkové soubory společné pro hlavní aplikaci i všechny moduly
#
COMMON_HEADERS = include/test.h include/libconfig.h
#
# společné knihovny pro hlavní aplikaci i moduly
#
COMMON_LIBS = -L$(DESTDIR) -lconfig
#
#------------------------------------------------------
# parametry pro hlavní aplikaci
#------------------------------------------------------
#
# seznam zdrojových souborů v adresáři 'benchmark'
#
MAIN_SRC = main.c
#
# seznam hlavičkových souborů v adresáři 'benchmark'
#
MAIN_H =
#
# příznaky pro kompilaci a linkování hlavní aplikace
#
MAIN_CFLAGS = -Wall -c
MAIN_LFLAGS = -Wl,-rpath='$$ORIGIN'
#
# seznam použitých knihoven
#
MAIN_LIBS = dl
#
#------------------------------------------------------
# parametry pro jednotlivé moduly
#------------------------------------------------------
#
# seznam modulů
#
MODULES = sort net file sock mem
#
#------------------------------------------------------
# následuje sekce, ve které definujeme pro každý modul
# seznam zdrojových a hlavičkových souborů, dodatečné
# příznaky pro kompilaci a linkování a seznam použitých
# knihoven
#
# např. pro modul 'example' definujeme proměnné
#   example_SRC - zdrojové soubory v adresáři 'example'
#   example_H - hlavičkové soubory v adresáři 'example'
#   example_CFLAGS - pro soubory *.c
#   example_LFLAGS
#   example_LIBS
#------------------------------------------------------
# modul 'mem'
#
mem_SRC = main.c
mem_H =
mem_CFLAGS =
mem_LFLAGS =
mem_LIBS = -pthread
#------------------------------------------------------
# modul 'sort'
#
sort_SRC = main.c
sort_H =
sort_CFLAGS =
sort_LFLAGS =
sort_LIBS =
#------------------------------------------------------
# modul 'net'
#
net_SRC = main.c common.c socket.c
net_H = common.h socket.h
net_CFLAGS =
net_LFLAGS =
net_LIBS =
#------------------------------------------------------
# modul 'sock'
#
sock_SRC = main.c
sock_H =
sock_CFLAGS =
sock_LFLAGS =
sock_LIBS =
#------------------------------------------------------
# modul 'file'
#
file_SRC = main.c
file_H =
file_CFLAGS =
file_LFLAGS =
file_LIBS = -pthread
#------------------------------------------------------
#
# Promenne pro preklad sdilene knihovny pro zpracovani
# konfiguracniho souboru
#------------------------------------------------------
LIBCONFIG_C = grammar.c libconfig.c scanctx.c scanner.c strbuf.c
LIBCONFIG_H = libconfig/grammar.h libconfig/parsectx.h libconfig/scanctx.h libconfig/scanner.h libconfig/strbuf.h libconfig/wincompat.h include/libconfig.h
LIBCONFIG_OBJS = $(patsubst %.c,$(DESTDIR)/libconfig/%.o,$(LIBCONFIG_C))
####################################################################
# definice pravidel a závislostí
####################################################################
#
# all - základní cíl pro sestavení aplikace a modulů
#

.PHONY: all

all: $(DESTDIR)/libconfig.so $(DESTDIR)/benchmark $(patsubst %,$(DESTDIR)/bm_%.so,$(MODULES)) | $(DESTDIR)/.

# vytvoření adresáře $(DESTDIR) pokud ještě neexistuje
$(DESTDIR)/.:
	mkdir -p $(DESTDIR)


#------------------------------------------------------
# sestaveni knihovny libconfig
#

$(DESTDIR)/libconfig.so: $(LIBCONFIG_OBJS)
	$(CC) -shared -o $@ $?
$(DESTDIR)/libconfig/.:
	mkdir -p $(DESTDIR)/libconfig
$(DESTDIR)/libconfig/%.o: libconfig/%.c $(LIBCONFIG_H) | $(DESTDIR)/libconfig/.
	$(CC) $(SO_CFLAGS) -o $@ $<


#------------------------------------------------------
# sestavení hlavní aplikace
#

# vytvoření seznamu objektů
MAIN_OBJS = $(patsubst %.c,$(DESTDIR)/main/%.o,$(MAIN_SRC))
# linkování
$(DESTDIR)/benchmark: $(MAIN_OBJS) $(DESTDIR)/libconfig.so
	$(CC) $(MAIN_LFLAGS) -o $@ $(MAIN_OBJS) $(addprefix -l,$(MAIN_LIBS)) $(COMMON_LIBS)
# kontrola existence cílových adresářů (ale bez závislosti na času modifikace)
$(MAIN_OBJS): | $(DESTDIR)/main/.
# vytvoření cílového adresáře pro objektové soubory
$(DESTDIR)/main/.:
	mkdir -p $(DESTDIR)/main
# kompilace jednotlivých souborů
$(DESTDIR)/main/%.o: benchmark/%.c $(patsubst %,benchmark/%,$(MAIN_H)) $(COMMON_HEADERS)
	$(CC) $(MAIN_CFLAGS) -o $@ $<

#------------------------------------------------------
# sestavení modulů probíhá pomocí jednotné šablony
#   parametr - jméno modulu
#

# definice šablony
define module_TEMPLATE
# vytvoření seznamu hlaviček
$(1)_HEADERS = $$(patsubst %,$(1)/%,$$($(1)_H))
# vytvoření seznamu objektů
$(1)_OBJS = $$(patsubst %.c,$$(DESTDIR)/$(1)/%.o,$$($(1)_SRC))
# linkování
$$(DESTDIR)/bm_$(1).so: $$($(1)_OBJS) $$(DESTDIR)/libconfig.so
	$$(CC) $$(SO_LFLAGS) $$($(1)_LFLAGS) -o $$@ $$($(1)_OBJS) $$($(1)_LIBS) $$(COMMON_LIBS)
# kontrola existence cílových adresářů (ale bez závislosti na času modifikace)
$$($(1)_OBJS): | $$(DESTDIR)/. $$(DESTDIR)/$(1)/.
# vytvoření cílového adresáře pro objektové soubory
$$(DESTDIR)/$(1)/.:
	mkdir $$(DESTDIR)/$(1)
# kompilace jednotlivých souborů
$$(DESTDIR)/$(1)/%.o: $(1)/%.c $$($(1)_HEADERS) $$(COMMON_HEADERS)
	$$(CC) $$(SO_CFLAGS) $$($(1)_CFLAGS) -o $$@ $$<
endef

# vytvoření pravidel pro každý modul pomocí šablony
$(foreach module,$(MODULES),$(eval $(call module_TEMPLATE,$(module))))

####################################################################
#
# clean - vymazání všech produktů kompilace a linkování
#

.PHONY: clean

clean:
	rm -f $(DESTDIR)/*/*.o
	rm -f $(DESTDIR)/benchmark $(DESTDIR)/bm_*.so

####################################################################

