#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include "../include/test.h"

#define BUF_SIZE 8388608 // 8 MB
#define FILE_NAME_LENGTH 50

struct params_t {
    int buf_use_count;
    int seed;
    int threads;
};

static struct params_t params;
static struct test_t tests[2];
static bool verbose;
static pthread_t *threads;
static uint8_t buf[BUF_SIZE];
static const char *filename = "/tmp/benchmark_file_test_%d";

static const char *last_error;
static pthread_mutex_t synch_mutex;
static pthread_cond_t synch_cv;
static bool synch_wakeup;

static struct test_suite suite;



void *
write_routine(void *number)
{
    int fd;
    int i;

    pthread_mutex_lock(&synch_mutex);
    while (!synch_wakeup) {
        pthread_cond_wait(&synch_cv, &synch_mutex);
    }
    pthread_mutex_unlock(&synch_mutex);

    char name[FILE_NAME_LENGTH];
    int num = *(int *)number;
    snprintf(name, FILE_NAME_LENGTH, filename, num);
    // printf("thread id: %d\n", (unsigned)number);
    fd = open(name, O_WRONLY | O_CREAT, 0664);
    if (fd == 0) {
        last_error = "failed create output file";
        pthread_exit(NULL);
    }
    for (i = 0; i < params.buf_use_count; i++) {
        write(fd, (const void *)buf, BUF_SIZE);
    }
    free((int *)number);
    close(fd);
    pthread_exit(NULL);
}

void *
read_routine(void *number)
{
    int fd;
    int i;

    pthread_mutex_lock(&synch_mutex);
    while (!synch_wakeup) {
        pthread_cond_wait(&synch_cv, &synch_mutex);
    }
    pthread_mutex_unlock(&synch_mutex);

    // uint8_t buffer[BUF_SIZE];
    uint8_t *buffer = malloc(BUF_SIZE * sizeof (uint8_t));
    if (buffer == NULL) {
        pthread_exit(NULL);
    }
    char name[FILE_NAME_LENGTH];
    int num = *(int *)number;
    snprintf(name, FILE_NAME_LENGTH, filename, num);
    // printf("thread id: %d\n", (unsigned)number);
    fd = open(name, O_RDONLY);
    if (fd == 0) {
        last_error = "failed open file";
        pthread_exit(NULL);
    }
    for (i = 0; i < params.buf_use_count; i++) {
        read(fd, (void *)buffer, BUF_SIZE);
    }
    free((int *)number);
    close(fd);
    free(buffer);
    pthread_exit(NULL);
}

// -----WRITE-----
void
write_init() {}

const char *
write_get_info()
{
    return ("write - thread files");
}

bool
write_prepare_environment()
{
    int i;
    srand(params.seed);
    for (i = 0; i < BUF_SIZE; i++) {
        buf[i] = (uint8_t)rand();
    }
    threads = (pthread_t *)malloc(params.threads * sizeof (pthread_t));
    if (threads == NULL) {
        last_error = "allocation failed";
        return (false);
    }

    pthread_mutex_init(&synch_mutex, NULL);
    pthread_cond_init(&synch_cv, NULL);
    synch_wakeup = false;

    int retval;
    for (i = 0; i < params.threads; i++) {
        int *arg = (int *)malloc(sizeof (int));
            if (arg == NULL) {
                last_error = "allocation thread id variable failed";
                free(threads);
                return (false);
            }
        *arg = i;
        retval = pthread_create(&threads[i], NULL, write_routine, arg);
        if (retval != 0) {
            last_error = "failed create threads";
            return (false);
        }
    }

    if (verbose) {
        printf("  overall data size: %dB\n", params.buf_use_count * BUF_SIZE);
        printf("  threads: %d\n", params.threads);
    }
    return (true);
}

void
write_run_test()
{
    int i;

    pthread_mutex_lock(&synch_mutex);
    synch_wakeup = true;
    pthread_cond_broadcast(&synch_cv);
    pthread_mutex_unlock(&synch_mutex);

    for (i = 0; i < params.threads; i++) {
        pthread_join(threads[i], NULL);
    }
}

void
write_evaluate(double time)
{
    printf("Evaluated:\n  overall speed: %f Mbit/s\n",
        (params.buf_use_count * BUF_SIZE / 1024 / 1024 * params.threads *
        8) / time);
}

bool
write_check_result()
{
    int i;
    struct stat file_info;
    char name[FILE_NAME_LENGTH];
    for (i = 0; i < params.threads; i++) {
        snprintf(name, FILE_NAME_LENGTH, filename, i);
        if (lstat(name, &file_info) == -1) {
            last_error = "reading file failed";
            return (false);
        }
        if ((int)file_info.st_size != BUF_SIZE * params.buf_use_count) {
            last_error = "wrong file size";
            return (false);
        }
    }
    return (true);
}

const char *
write_get_last_error()
{
    return (last_error);
}

bool
write_clear_environment()
{
    char name[FILE_NAME_LENGTH];
    int i;
    for (i = 0; i < params.threads; i++) {
        snprintf(name, FILE_NAME_LENGTH, filename, i);
        unlink(name);
    }
    free(threads);
    return (true);
}

void
write_deinit() {}


// -----READ-----
void
read_init() {}

const char *
read_get_info()
{
    return ("read - thread files");
}

bool
read_prepare_environment()
{
    int fd;
    int i;
    int j;
    char name[FILE_NAME_LENGTH];

    srand(params.seed);
    for (i = 0; i < BUF_SIZE; i++) {
        buf[i] = (uint8_t)rand();
    }
    threads = (pthread_t *)malloc(params.threads * sizeof (pthread_t));
    if (threads == NULL) {
        last_error = "allocation failed";
        return (false);
    }
    for (i = 0; i < params.threads; i++) {
        snprintf(name, FILE_NAME_LENGTH, filename, i);
        fd = open(name, O_WRONLY | O_CREAT, 0664);
        if (fd == 0) {
            last_error = "failed create output file";
            return (false);
        }
        for (j = 0; j < params.buf_use_count; j++) {
            write(fd, (const void *)buf, BUF_SIZE);
        }
        close(fd);
    }

    pthread_mutex_init(&synch_mutex, NULL);
    pthread_cond_init(&synch_cv, NULL);
    synch_wakeup = false;

    int retval;
    for (i = 0; i < params.threads; i++) {
        int *arg = (int *)malloc(sizeof (int));
            if (arg == NULL) {
                last_error = "allocation thread id variable failed";
                free(threads);
                return (false);
            }
        *arg = i;
        retval = pthread_create(&threads[i], NULL, read_routine, arg);
        if (retval != 0) {
            last_error = "failed create threads";
            return (false);
        }
    }

    if (verbose) {
        printf("  overall data size: %dB\n", params.buf_use_count * BUF_SIZE);
        printf("  threads: %d\n", params.threads);
    }
    return (true);
}

void
read_run_test()
{
    int i;

    pthread_mutex_lock(&synch_mutex);
    synch_wakeup = true;
    pthread_cond_broadcast(&synch_cv);
    pthread_mutex_unlock(&synch_mutex);

    for (i = 0; i < params.threads; i++) {
        pthread_join(threads[i], NULL);
    }
}

void
read_evaluate(double time)
{
    printf("Evaluated:\n  overall speed: %f Mbit/s\n",
        (params.buf_use_count * BUF_SIZE / 1024 / 1024 * params.threads *
        8) / time);
}

bool
read_check_result()
{
    return (true);
}

const char *
read_get_last_error()
{
    return (last_error);
}

bool
read_clear_environment()
{
    char name[FILE_NAME_LENGTH];
    int i;
    for (i = 0; i < params.threads; i++) {
        snprintf(name, FILE_NAME_LENGTH, filename, i);
        unlink(name);
    }
    free(threads);
    return (true);
}

void
read_deinit() {}


void
file_print_params()
{
    printf("   * Params:\n       buf_use_count: %d\n       seed: \
%d\n       threads: %d\n       buffer size: %dB\n",
        params.buf_use_count, params.seed, params.threads, BUF_SIZE);
}


struct test_suite *
create(bool verbose_flag, config_t *cfg)
{
    if (!config_lookup_int(cfg, "file_threads", &params.threads)) {
        params.threads = 8;
    }
    if (!config_lookup_int(cfg, "file_seed", &params.seed)) {
        params.seed = 4569;
    }
    if (!config_lookup_int(cfg, "file_buf_use_count", &params.buf_use_count)) {
        params.buf_use_count = 10;
    }

    struct test_t *test;
    test = tests;

    // write
    test->check_result = write_check_result;
    test->clear_environment = write_clear_environment;
    test->deinit = write_deinit;
    test->evaluate = write_evaluate;
    test->get_info = write_get_info;
    test->get_last_error = write_get_last_error;
    test->init = write_init;
    test->prepare_environment = write_prepare_environment;
    test->run_test = write_run_test;

    test++;

    // read
    test->check_result = read_check_result;
    test->clear_environment = read_clear_environment;
    test->deinit = read_deinit;
    test->evaluate = read_evaluate;
    test->get_info = read_get_info;
    test->get_last_error = read_get_last_error;
    test->init = read_init;
    test->prepare_environment = read_prepare_environment;
    test->run_test = read_run_test;

    verbose = verbose_flag;

    suite.name = "file - thread IO";
    suite.is_valid_conf = true;
    suite.test_count = 2;
    suite.print_params = file_print_params;
    suite.tests = tests;

    return (&suite);
}
