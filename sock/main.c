#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <semaphore.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "../include/test.h"


struct params_t {
    int items;
    int seed;
};

static struct params_t params;
static struct test_t tests[2];
static bool verbose;

static uint8_t *child_buffer = NULL;
static uint8_t *data = NULL;
static uint8_t *rec_data = NULL;
static unsigned buf_size = 0;
static const char *last_error;
static int sockets[2];

static struct test_suite suite;



void
clean()
{
    free(child_buffer);
    free(data);
    free(rec_data);
    child_buffer = data = rec_data = NULL;
}


void
init_2()
{
    buf_size = 2 * 1024;
}

void
init_32()
{
    buf_size = 32 * 1024;
}

const char *
get_info_2()
{
    return ("sock2 - unix socket in 2kB blocks");
}

const char *
get_info_32()
{
    return ("sock32 - unix socket in 32kB blocks");
}

bool
prepare_environment()
{
    child_buffer = (uint8_t *)malloc(buf_size * sizeof (uint8_t));
    data = (uint8_t *)malloc(params.items * sizeof (uint8_t));
    rec_data = (uint8_t *)malloc(params.items * sizeof (uint8_t));

    if (child_buffer == NULL || data == NULL || rec_data == NULL) {
        last_error = "allocation data failed";
        clean();
        return (false);
    }
    srand(params.seed);
    int i;
    for (i = 0; i < params.items; i++) {
        data[i] = (uint8_t)rand();
    }
    if (socketpair(AF_UNIX, SOCK_STREAM, 0, sockets) < 0) {
        last_error = "creating sockets failed";
        clean();
        return (false);
    }
    if (verbose) {
        printf("  data size: %dB\n", params.items);
        printf("  block size: %dB\n", buf_size);
    }
    return (true);
}

void
run_test()
{
    int child;
    if ((child = fork()) == -1) {
        last_error = "fork failed";
        return;
    } else if (child == 0) { // child
        int received_total = 0;
        free(data);
        free(rec_data);
        close(sockets[1]);
        while (received_total < params.items) {
            int received_now = 0;
            while (received_now != (int)buf_size) {
                received_now += recv(sockets[0], (void*)child_buffer +
                    received_now, (size_t)buf_size - received_now, 0);
            }
            if (received_now > 0) {
                received_total += received_now;
            } else {
                break;
            }

            unsigned send_now = 0;
            while (send_now != buf_size) {
                send_now += send(sockets[0], (const void*)child_buffer +
                    send_now, (size_t)buf_size - send_now, 0);
            }
            if (send_now != buf_size) {
                break;
            }
        }
        free(child_buffer);
        close(sockets[0]);
        exit(0);
    } else { // parent
        int send_total = 0;
        uint8_t *temp_data = data;
        uint8_t *temp_rec_data = rec_data;
        close(sockets[0]);
        while (send_total < params.items) {
            int send_now = 0;
            while (send_now != (int)buf_size) {
                send_now += send(sockets[1], (const void*)temp_data + send_now,
                        (size_t)buf_size - send_now, 0);
            }
            if (send_now > 0) {
                send_total += send_now;
                temp_data += send_now;
            } else {
                break;
            }

            int received_now = 0;
            while (received_now != (int)buf_size) {
                received_now += recv(sockets[1], (void*)temp_rec_data +
                    received_now, (size_t)buf_size - received_now, 0);
            }
            if (received_now > 0) {
                temp_rec_data += received_now;
            } else {
                break;
            }
        }
        wait(NULL);
        close(sockets[1]);
    }
}

void
evaluate(double time)
{
    printf("Evaluated:\n  speed: %f Mbit/s\n", (params.items / 1024 / 1024 *
        2 * 8) / time);
}

bool
check_result()
{
    if (memcmp(data, rec_data, params.items) == 0) {
        return (true);
    } else {
        return (false);
    }
}

const char *
get_last_error()
{
    return (last_error);
}

bool
clear_environment()
{
    clean();
    // close(sockets[0]);
    // close(sockets[1]);
    return (true);
}

void
deinit() {}


void
sock_print_params()
{
    printf("   * Params:\n       items: %d\n       seed: %d\n",
        params.items, params.seed);
}


struct test_suite *
create(bool verbose_flag, config_t *cfg)
{
    if (!config_lookup_int(cfg, "sock_items", &params.items)) {
        params.items = 8388608; // 8 MB
    }
    if (!config_lookup_int(cfg, "sock_seed", &params.seed)) {
        params.seed = 4567;
    }

    struct test_t *test;
    test = tests;

    // sock_2
    test->init = init_2;
    test->check_result = check_result;
    test->clear_environment = clear_environment;
    test->evaluate = evaluate;
    test->get_info = get_info_2;
    test->get_last_error = get_last_error;
    test->prepare_environment = prepare_environment;
    test->run_test = run_test;
    test->deinit = deinit;

    test++;

    // sock_32
    test->init = init_32;
    test->check_result = check_result;
    test->clear_environment = clear_environment;
    test->evaluate = evaluate;
    test->get_info = get_info_32;
    test->get_last_error = get_last_error;
    test->prepare_environment = prepare_environment;
    test->run_test = run_test;
    test->deinit = deinit;

    verbose = verbose_flag;

    suite.name = "sock - unix socket interprocess communication";
    suite.is_valid_conf = true;
    suite.test_count = 2;
    suite.print_params = sock_print_params;
    suite.tests = tests;

    return (&suite);
}
