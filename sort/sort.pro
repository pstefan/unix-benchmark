TEMPLATE = lib
TARGET = bm_sort
CONFIG += plugin no_plugin_name_prefix

DESTDIR = ../debug

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -fPIC
QMAKE_LFLAGS += -shared

SOURCES += main.c

HEADERS += \
    ../include/test.h \
    ../include/libconfig.h
