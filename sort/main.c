#include <stdio.h>
#include <stdlib.h>
#include "../include/test.h"

struct params_t {
    int items;
    int seed;
};


static bool verbose;
static const char *last_error;
static struct params_t params;
static struct test_t tests[1];
static unsigned *data;

static struct test_suite suite;

int
compare(const void *a, const void *b)
{
    if (*(unsigned *)a < *(unsigned *)b)
        return (-1);
    else if (*(unsigned *)a == *(unsigned *)b)
        return (0);
    else
        return (1);
}

void
sort_init() {}

const char *
sort_get_info()
{
    return ("quicksort - sorting vector of items");
}

bool
sort_prepare_environment()
{
    int i;
    data = (unsigned *)malloc(params.items * sizeof (unsigned));
    srand(params.seed);

    for (i = 0; i < params.items; i++) {
        data[i] = (unsigned)rand();
    }
    if (verbose) {
        printf("  items: %d\n", params.items);
        printf("  item lenght: %ld\n", sizeof (unsigned));
        printf("  seed: %d\n", params.seed);
    }
    return (true);
}

void
sort_run_test()
{
    qsort(data, params.items, sizeof (unsigned), compare);
}

bool
sort_check_result()
{
    unsigned last = data[0];
    unsigned current;
    int i;

    for (i = 1; i < params.items; i++) {
        current = data[i];
        if (last > current) {
            last_error = "Sorting test failed.";
            return (false);
        }
        last = current;
    }
    return (true);
}

void
sort_evaluate() {}

const char *
sort_get_last_error()
{
    return (last_error);
}

bool
sort_clear_environment()
{
    free(data);
    return (true);
}

void
sort_deinit() {}


void
sort_print_params()
{
    printf("   * Params:\n       items: %d\n       seed: %d\n",
        params.items, params.seed);
}


struct test_suite *
create(bool verbose_flag, config_t *cfg)
{

    last_error = NULL;

    if (!config_lookup_int(cfg, "sort_items", &params.items)) {
        params.items = 400000;
    }
    if (!config_lookup_int(cfg, "sort_seed", &params.seed)) {
        params.seed = 4560;
    }

    struct test_t *test;
    test = tests;

    // sort
    test->init = sort_init;
    test->check_result = sort_check_result;
    test->clear_environment = sort_clear_environment;
    test->evaluate = sort_evaluate;
    test->get_info = sort_get_info;
    test->get_last_error = sort_get_last_error;
    test->prepare_environment = sort_prepare_environment;
    test->run_test = sort_run_test;
    test->deinit = sort_deinit;

    verbose = verbose_flag;

    suite.name = "sort - sorting data";
    suite.is_valid_conf = true;
    suite.test_count = 1;
    suite.print_params = sort_print_params;
    suite.tests = tests;

    return (&suite);
}
