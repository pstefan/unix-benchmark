#Unix benchmark
Tento projekt má za cíl otestovat výkon některých operací používajících UNIXové API. Program je implementován jako benchmark, který obsahuje hlavní aplikaci a jednotlivé moduly. Hlavní program se stará o interakci s uživatelem, načítání jednotlivých modulů a měření doby běhu.

##Překlad
Překlad hlavní aplikace i všech modulů pomocí GNU make a gcc je řízen přiloženým souborem *Makefile*. Vlastní překlad provedete spuštěním

	$ make
Přeložené soubory jsou v adresáři *bin/*.
Smazání výsledných binárních a objektových souborů provedete příkazem

	$ make clean

**Poznámka:** V případě vytváření nových modulů je užitečné číst komentáře v souboru *Makefile*.

##Použití
Program podporuje následující přepínače:

- *-h, --help* - vypíše krátkou nápovědu
- *-v, --verbose* - zapne mód s rozšířeným výpisem
- *-l, --list* - vypíše seznam dostupných testů
- *-t, --times* - nastaví počet opakování každého testu
- *-a, --all* - spustí všechny dostupné testy (toto je výchozí chování)
- *-i, --include* - spustí pouze vybrané testy
- *-e, --exclude* - spustí všechny testy kromě vyjmenovaných
- *-c, --config* - určí cestu ke konfiguračnímu souboru

Přepínače *-i* a *-e* vyžadují jako parametr seznam testů. Jejich jména jsou oddělena čárkou bez mezery. Jméno testu, který je v souboru *bm_net.so* je **net**.

Přepínače *-i*, *-e* a *-a* se vylučují!

###Příklady spuštění
	$ ./benchmark -i net,sock -v
	$ ./benchmark -e mem -l
	$ ./benchmark -c ./my_config.cfg

###Konfigurační soubor
K projektu je dodáván výchozí konfigurační soubor, *config.cfg*. Aplikace standartně hledá konfigurační soubor *config.cfg* v adresáři, který obsahuje binárku té aplikace. V případě, že tento konfigurační soubor chybí, použijí se výchozí hodnoty pro všechny parametry. Cestu k jinému konfiguračnímu souboru je možné specifikovat při spuštění aplikace pomocí přepínače *-c*.

Názvy konfigurovaných parametrů odpovídají názvům z kódu jednotlivých modulů, jejich název by měl být dostatečně popisný. Dodávaný soubor také obsahuje stručnou nápovědu.

Konfigurační soubor je parsovám pomocí knihovny [libconfig](http://www.hyperrealm.com/libconfig/). Její licence je uvedena na začátku každého přejatého souboru.

##Popis modulů
###Sort
Jednoduchý modul, který setřídí nějaká náhodná data knihovní implementací quicksortu.
###Mem
Tento modul testuje sdílenou paměť mezi procesy. Test probíhá tak, že proces provede *fork()* a tyto 2 procesy (rodič se synem) si přes sdílenou paměť vyměňují střídavě data. Synchronizace je zajištěna pomocí semaforu.
###Net
Modul net měří latenci a throughput síťového stacku. Obsahuje jak testy přes TCP tak přes UDP. Ke svému běhu potřebuje spuštěný síťový odpovídač *net-echo* na cílovém stroji (ve výchozím nastavení localhost). Jeho popisem se zabývá samostatná kapitola. Test probíhá tak, že se vždy odešlou data v bloku dané velikosti a čeká se na jejich návrat.
###Sock
Tento modul testuje meziprocesovou komunikaci přes UNIXové sockety. Při testování se opět provede *fork()* a oba procesy si posílají přes předem otevřené sockety připravené data.
###File
Modul file testuje diskové operace, konkrétně čtení a zápis do souborů. Test probíhá paralelně, každé vlákno má na starost jeden soubor. Ty se dočasně tvoří v adresáři */tmp/*, po dokončení testu se odstraní. Vlákna jsou tvořena pomocí *pthread_create()*.

##Net-echo
Net-echo je pomocný program, který poslouchá na všech dostupných síťových rozhraních na daném portu (výchozí je 10000) a přijatá data obratem odešle zpět. Podporuje i IPv6. Zdrojové soubory i samostatný *Makefile* jsou v adresáři *net-echo/*, do kterého se generuje i výsledný binární soubor. Program se ukončí stisknutím klávesové zkratky **Ctrl + C**.
###Přepínače

- *-h, --help* - vypíše krátkou nápovědu
- *-t, --tcp* - nastaví TCP port
- *-u, --udp* - nastaví UDP port