#ifndef TEST_H
#define TEST_H

#include <stdbool.h>
#include "libconfig.h"


typedef void (*init_t)();
typedef const char *(*get_info_t)();
typedef bool (*prepare_environment_t)();
typedef void (*run_test_t)();
typedef void (*evaluate_t)(double);
typedef bool (*check_result_t)();
typedef const char *(*get_last_error_t)();
typedef bool (*clear_environment_t)();
typedef void (*deinit_t)();

struct test_t {
    init_t init;
    get_info_t get_info;
    prepare_environment_t prepare_environment;
    run_test_t run_test;
    evaluate_t evaluate;
    check_result_t check_result;
    get_last_error_t get_last_error;
    clear_environment_t clear_environment;
    deinit_t deinit;
};

typedef void (*print_params_t)();

struct test_suite {
    const char *name;
    print_params_t print_params;
    unsigned test_count;
    struct test_t *tests;
    bool is_valid_conf;
};


typedef struct test_suite *(*create_t)(bool, config_t *);



#endif // TEST_H
