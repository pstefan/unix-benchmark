TEMPLATE = app

DESTDIR = ../debug

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_LFLAGS += -Wl,-rpath=\'\$\$ORIGIN\'

LIBS += -ldl

SOURCES += main.c

HEADERS += \
    ../include/test.h

