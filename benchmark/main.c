#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/types.h>
#include <libgen.h>
#include "../include/test.h"
#include "../include/libconfig.h"


static const char app_title[] = "UNIX Benchmark utility, Petr Stefan 2014";
static const char lib_prefix[] = "bm_";
static const char lib_suffix[] = ".so";

enum return_value {OK = 0, BAD_PARAM, LIB_LOAD, CONF_PARSE, BAD_CONF};

struct timespec
tdiff(struct timespec *start, struct timespec *end)
{
    struct timespec temp;
    if (end->tv_nsec < start->tv_nsec) {
        temp.tv_sec = end->tv_sec - start->tv_sec - 1;
        temp.tv_nsec = 1000000000 + end->tv_nsec - start->tv_nsec;
    } else {
        temp.tv_sec = end->tv_sec - start->tv_sec;
        temp.tv_nsec = end->tv_nsec - start->tv_nsec;
    }
    return (temp);
}

bool
is_valid_lib(const char *lib_name)
{
    int prefix_length;
    int suffix_length;
    bool result;
    const char *temp;

    result = true;
    prefix_length = strlen(lib_prefix);
    suffix_length = strlen(lib_suffix);

    if (strncmp(lib_name, lib_prefix, prefix_length) != 0) {
        result = false;
    }
    temp = lib_name + (strlen(lib_name) - suffix_length);
    if (strcmp(temp, lib_suffix) != 0) {
        result = false;
    }
    return (result);
}

char *
get_next_lib(DIR *dir)
{
    struct dirent *dirp;
    char *file = NULL;

    dirp = readdir(dir);

    while (dirp != NULL) {
        if (is_valid_lib(dirp->d_name)) {
            file = strdup(dirp->d_name);
            break;
        }
        dirp = readdir(dir);
    }
    return (file);
}

void
run_test(struct test_t *test, int verbose_flag, int times)
{
    clockid_t clockid;
    struct timespec cpuclk_begin, cpuclk_end, cpuclk_diff;
    struct timespec tbegin, tend, tdifference;
    bool result;
    int i;

    clock_getcpuclockid(0, &clockid);

    for (i = 0; i < times; i++) {
        if (verbose_flag == 1)
            printf("Test: %s\n", test->get_info());
        else {
            const char *end;
            const char *info;
            info = test->get_info();
            end = strchr(info, ' ');
            printf("%.*s ", (int)(end - info), info);
            fflush(stdout);
        }
        test->init();
        // priprava prostredi pro spusteni testu
        if (verbose_flag == 1)
            printf("Preparing environment: \n");
        result = test->prepare_environment();
        if (verbose_flag == 1) {
            if (result)
                printf("  success\n");
            else {
                printf("  failure\n%s\nQuiting...\n\n", test->get_last_error());
                break;
            }
        } else {
            if (!result) {
                printf("- preparing failure: %s\n", test->get_last_error());
                break;
            }
        }
        if (verbose_flag == 1)
            printf("Testing: \n");

        // zacatek mereni casu
        clock_gettime(CLOCK_MONOTONIC, &tbegin);
        clock_gettime(clockid, &cpuclk_begin);

        // spusteni testu
        test->run_test();

        // konec mereni casu
        clock_gettime(clockid, &cpuclk_end);
        clock_gettime(CLOCK_MONOTONIC, &tend);

        // spocitani rozdilu casu - doba behu
        tdifference = tdiff(&tbegin, &tend);
        cpuclk_diff = tdiff(&cpuclk_begin, &cpuclk_end);

        // vypsani casu
        if (verbose_flag == 1) {
            printf("  real time:\t%d.%09lld seconds\n",
                (int)tdifference.tv_sec, (long long)tdifference.tv_nsec);
            printf("  CPU time:\t%d.%09lld seconds\n",
                (int)cpuclk_diff.tv_sec, (long long)cpuclk_diff.tv_nsec);
        } else {
            printf("%d.%09lld %d.%09lld\n", (int)tdifference.tv_sec,
                (long long)tdifference.tv_nsec, (int)cpuclk_diff.tv_sec,
                (long long)cpuclk_diff.tv_nsec);
        }

        // kontrola spravnosti vysledku
        if (verbose_flag == 1)
            printf("Checking results: \n");
        result = test->check_result();
        if (verbose_flag == 1) {
            if (result)
                printf("  success\n");
            else {
                printf("  failure\n%s\n", test->get_last_error());
            }
        } else {
            if (!result) {
                printf("- checking failure: %s\n", test->get_last_error());
            }
        }

        // evaluate metoda muze spocitat a vypsat dodatecne informace -
        // napriklad rychlost zapisu dat, cas latence pro jeden paket atd
        // Vola se jen ve verbose modu pri uspesnem dokonceni testu.
        // Jako parametr se predava namereny real time.
        if (verbose_flag == 1 && result) {
            test->evaluate(tdifference.tv_sec + tdifference.tv_nsec /
                1000000000.0);
        }

        // uklid
        if (verbose_flag == 1)
            printf("Cleaning environment: \n");
        result = test->clear_environment();
        if (verbose_flag == 1) {
            if (result)
                printf("  success\n");
            else {
                printf("  failure\n%s\nQuiting...\n\n",
                    test->get_last_error());
            }
            printf("\n");
        } else {
            if (!result) {
                printf("- cleaning failure: %s\n", test->get_last_error());
            }
        }
        test->deinit();
    }
}

void
print_tests_header()
{
    int i;
    for (i = 0; i < 80; i++) {
        printf("-");
    }
    printf("\n%-55s%-25s\n", "Module", "File");
    for (i = 0; i < 80; i++) {
        printf("-");
    }
    printf("\n");
}

void
print_tests_footer()
{
    int i;
    for (i = 0; i < 80; i++) {
        printf("-");
    }
    printf("\n");
}

void
print_tests(struct test_suite *tests,
    const char *current_lib, int verbose_flag)
{
    unsigned i;

    printf("%-55s%-25s\n", tests->name, current_lib);
    if (verbose_flag == 1) {
        for (i = 0; i < tests->test_count; i++) {
            printf("   # %s\n", tests->tests[i].get_info());
        }
        tests->print_params();
    }
}

bool
is_used_lib(const char *current_lib,
    const char *include, const char *exclude)
{
    bool retval = true;
    char *buf;
    int prefix_length = strlen(lib_prefix);
    int suffix_length = strlen(lib_suffix);
    int lib_name_length = strlen(current_lib);
    buf = (char *)malloc((lib_name_length - prefix_length -
        suffix_length + 1) * sizeof (char));
    strncpy(buf, current_lib + prefix_length,
        lib_name_length - prefix_length - suffix_length + 1);
    buf[lib_name_length - prefix_length - suffix_length] = '\0';

    if (exclude != NULL) {
        if (strstr(exclude, buf) != NULL) {
            retval = false;
        }
    } else if (include != NULL) {
        if (strstr(include, buf) == NULL) {
            retval = false;
        }
    }

    free(buf);
    return (retval);
}

int
main(int argc, char **argv)
{
    int verbose_flag = 0;
    int list_flag = 0;
    int all_flag = 0;
    int help_flag = 0;
    int times = 1;
    char *include = NULL;
    char *exclude = NULL;
    char *config_file = NULL;
    char *program_path = NULL;
    char *current_lib = NULL;
    struct option long_options[] = {
        {"verbose", no_argument, &verbose_flag, 1},
        {"list", no_argument, &list_flag, 1},
        {"all", no_argument, &all_flag, 1},
        {"help", no_argument, &help_flag, 1},
        {"times", required_argument, 0, 't'},
        {"include", required_argument, 0, 'i'},
        {"exclude", required_argument, 0, 'e'},
        {"config", required_argument, 0, 'c'},
        {0, 0, 0, 0}
    };
    DIR *dir;
    config_t cfg;


    int option_index;
    int opt;
    while (true) {
        opt = getopt_long(argc, argv, "vlat:i:e:c:h",
            long_options, &option_index);
        if (opt == -1)
            break;
        else if (opt == '?' || opt == ':')
            return (BAD_PARAM);

        switch (opt) {
            char *end;
        case 0:
            // tenhle dlouhy prepinac nastavuje nejaky flag
            break;
        case 'v':
            verbose_flag = 1;
            break;
        case 'l':
            list_flag = 1;
            break;
        case 'a':
            all_flag = 1;
            break;
        case 'h':
            help_flag = 1;
            break;
        case 't':
            times = (int) strtol(optarg, &end, 10);
            if (errno == ERANGE || end == optarg) {
                fprintf(stderr, "Option -t argument is wrong.\n");
                return (BAD_PARAM);
            }
            break;
        case 'i':
            include = optarg;
            break;
        case 'e':
            exclude = optarg;
            break;
        case 'c':
            config_file = optarg;
            break;
        default:
            break;
        }
    }


    // prepinace "all", "include" a "exclude" se vylucuji
    bool a = all_flag == 0 ? false : true;
    bool b = include == NULL ? false : true;
    bool c = exclude == NULL ? false : true;
    if ((a && b) || (b && c) || (c && a)) {
        fprintf(stderr, "Options '--all', '--include' and '--exclude'\
            are mutually exclusive.\n");
        return (BAD_PARAM);
    }

    fprintf(stdout, "%s\n", app_title);

    // vypis helpu
    if (help_flag == 1) {
        fprintf(stdout, "  %-15s%s\n", "-h, --help",
            "print this help page");
        fprintf(stdout, "  %-15s%s\n", "-v, --verbose",
            "set verbose output style");
        fprintf(stdout, "  %-15s%s\n", "-l, --list",
            "print list of available tests");
        fprintf(stdout, "  %-15s%s\n", "-t, --times",
            "repetitions to each test");
        fprintf(stdout, "  %-15s%s\n", "-a, --all",
            "run all available tests (default)");
        fprintf(stdout, "  %-15s%s\n", "-i, --include",
            "tests to run");
        fprintf(stdout, "  %-15s%s\n", "-e, --exclude",
            "tests not to run");
        fprintf(stdout, "  %-15s%s\n", "-c, --config",
            "path to config file");
        return (OK);
    }


    program_path = dirname(argv[0]);


    dir = opendir(program_path);
    if (dir == NULL) {
        fprintf(stderr, "Cannot open directory %s for loading tests.\n",
            program_path);
        return (LIB_LOAD);
    }

    char conf_filename[256];
    if (config_file == NULL) {
        strcpy(conf_filename, program_path);
        strcat(conf_filename, "/config.cfg");
    } else {
        strcpy(conf_filename, config_file);
    }
    config_init(&cfg);
    if (!config_read_file(&cfg, conf_filename)) {
        // fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
        //     config_error_line(&cfg), config_error_text(&cfg));
        printf("  Using default values\n");
        // misto ukonceni programu se pouziji vychozi hodnoty v modulech
        // config_destroy(&cfg);
        // return (CONF_PARSE);
    } else {
        printf("  Using config file %s\n", conf_filename);
    }


    if (list_flag == 1) {
        print_tests_header();
    }

    while ((current_lib = get_next_lib(dir)) != NULL) {
        if (!is_used_lib(current_lib, include, exclude)) {
            free(current_lib);
            continue;
        }
        void *handle = dlopen(current_lib, RTLD_NOW);
        if (handle == NULL) {
            fprintf(stderr, "Cannot open library '%s': %s\n",
                current_lib, dlerror());
        } else {
            create_t create = (create_t) dlsym(handle, "create");
            if (create == NULL)
                fprintf(stderr, "Failed to link 'create' function in\
                    %s: %s\n", current_lib, dlerror());
            else {
                struct test_suite *suite;
                suite = create(verbose_flag != 0, &cfg);
                if (suite->is_valid_conf) {
                    if (list_flag == 1) {
                        print_tests(suite, current_lib, verbose_flag);
                    } else {
                        unsigned i;
                        for (i = 0; i < suite->test_count; i++) {
                            run_test(&(suite->tests[i]), verbose_flag, times);
                        }
                    }
                } else {
                    fprintf(stderr, "Library %s: bad configuration.\n",
                        current_lib);
                }
            }
        }
        dlclose(handle);
        free(current_lib);
    }
    closedir(dir);
    config_destroy(&cfg);

    if (list_flag == 1) {
        print_tests_footer();
    }


    return (OK);
}
